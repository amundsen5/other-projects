import random


def program():
    #s = tails
    #g = heads
    print("This is a coin tosser!")
    numOfGoes = input("How sure do you want to be? Input a large number for more calculations: ")
    intInput = int(numOfGoes)
    numofheads = 0
    numoftails = 0
    for i in range(0, intInput):
        randomiser = random.randrange(0, 2)
        odds = "unknown"
        if randomiser == 0:
            odds = "heads"
        elif randomiser == 1:
            odds = "tails"

        if "heads" in odds:
            numofheads += 1
        elif "tails" in odds:
            numoftails += 1

    numofheadsper = numofheads / intInput * 100
    numoftailsper = numoftails / intInput * 100

    if numofheads == numoftails:
        print("you got 50/50")
        print(" ")
        input()
        program()
    elif numofheads < numoftails:
        print("The coin is %d%% tails" % numoftailsper)
        print(" ")
        input()
        program()
    elif numofheads > numoftails:
        print("The coin is %d%% heads" % numofheadsper)
        print(" ")
        input()
        program()
program()
