def program():
    inpoot = input("Code or Decode, c/d: ")
    if inpoot == "c":
        def encode():
            alphaNumeric = {'a': 1, 'b': 2, 'c': 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9, "j": 10, "k": 11,
                            "l": 12, "m": 13, "n": 14, "o": 15, "p": 16, "q": 17, "r": 18, "s":19, "t": 20, "u": 21,
                            "v": 22, "w": 23, "x": 24, "y": 25, "z": 26, " ": 0, ".": 28}

            inp = input("Input your sentence to be encoded: ")
            ex = list(inp)
            length = len(inp)

            for blue in range(0, length):
                red = ex[blue]
                coded = alphaNumeric[red]
                print(coded*11296000, end="-")
            print(" ")
            print(" --Little bug: Do not copy the '-' at the end of the coded word-number, it will break decoder :( --")
            print("press enter to return")
        encode()
        input()
        program()
    elif inpoot == "d":
        def decode():
            alphaNumeric = {"1": "a", "2": "b", "3": "c", "4": "d", "5": "e", "6": "f", "7": "g", "8": "h", "9": "i" ,
                            "10": "j", "11": "k", "12": "l", "13": "m", "14": "n", "15": "o", "16": "p", "17": "q",
                            "18": "r", "19": "s", "20": "t", "21": "u", "22": "v", "23": "w", "24": "x", "25": "y",
                            "26": "z", "0": "~", "28": "."}

            stringIn = input("Please input your code to decode: ")
            realLength1 = stringIn.split("-")
            realLength2 = len(realLength1)
            lenth = realLength2
            seperatedToArray = stringIn.split("-")
            for gi in range(0, lenth):
                runThrough = seperatedToArray[gi]
                toInt = int(runThrough)
                calculation = toInt / 11296000
                result = int(calculation)
                toStr = str(result)
                end = alphaNumeric[toStr]
                print(end, end="")
        decode()
        input()
        program()
    else:
        print("Wrong input")
        program()
program()